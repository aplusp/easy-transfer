# easy-transfer API

`easy-transfer` mimics a simple system for sending money between users registered to the platform.
The following assumptions were the foundations for the design and implementation.

* No Currency concept have been introduced to keep things super simple.
* The amounts of money tracked by the system have a 2 decimals scale and are rounded towards the "nearest neighbor"
 unless both neighbors are equidistant, in which case, round towards the even neighbor.
* An user (a physical entity) can be registered by providing basic personal informations. The email acts as an unique identifier
* An user owns potentially more than one account (unless adding account feature will not be implemented)
* An account is linked to only one User
* An account cannot have a negative balance

## Build the project and run the tests

    ./gradlew build

## Run the application

The following command will start an HTTP server listening on port `7000`

    ./gradlew :run

# REST API

The REST API is described below.

## Creates a new user

### Request

`POST /user`

    curl -X POST http://localhost:7000/user \
      --data @- << EOF
    {
    	"email" :"anemail@email.com",
    	"firstName" : "John",
    	"lastName": "Doe",
    	"phoneNumber" : "7930677845"
    }
    EOF

### Response

    HTTP/1.1 201 OK
    
    {
      "userId": "1759296d-cc7f-4abc-aeac-1f20c39e5248",
      "email" :"anemail@email.com",
      "firstName" : "John",
      "lastName": "Doe",
      "phoneNumber" : "7930677845"
      "createdOn": "2020-02-12T03:41:33.950Z",
      "accounts": [
          {
             "accountId": "4ce1d1f1-d186-492e-821e-393389f80447"
          }
       ]
    }
    
    
## Get an user

### Request

`GET http://localhost:7000/user/1759296d-cc7f-4abc-aeac-1f20c39e5248`

### Response

    HTTP/1.1 200 OK
    
    {
      "userId": "1759296d-cc7f-4abc-aeac-1f20c39e5248",
      "email" :"anemail@email.com",
      "firstName" : "John",
      "lastName": "Doe",
      "phoneNumber" : "7930677845"
      "createdOn": "2020-02-12T03:41:33.950Z",
      "accounts": [
          {
             "accountId": "4ce1d1f1-d186-492e-821e-393389f80447"
          }
       ]
    }
    
## Get an account

### Request

`GET /account`

    curl http://localhost:7000/account/30a5e9d1-3a7a-48ee-b45b-26835a4c1243

### Response

    HTTP/1.1 201 OK
    
    {
        "accountId": "30a5e9d1-3a7a-48ee-b45b-26835a4c1243",
        "currentBalance": 100.00,
        "state": "ACTIVE",
        "createdAt": "2020-02-12T05:05:36.013Z",
        "updatedAt": "2020-02-12T05:05:48.706Z"
    }
    
    
## Deposit money to an account

### Request

`POST /pay`

    curl -X POST http://localhost:7000/pay \
      --data @- << EOF
    {
    	"amount": 100.00,
        "targetAccount": "4ce1d1f1-d186-492e-821e-393389f80447",
        "reference": " a reference for the deposit"
    }
    EOF

### Response

    HTTP/1.1 200 OK
    
    {
      "success" : true,
      "detail" : {
          "transactionId" : "4ce4d1f1-895g-492e-821e-393389f80908",
          "state" : "SUCCESS",
          "completedAt" : "2020-02-12T03:41:33.950Z"
      }
    }   
    
     
## Transfer money between two accounts

### Request

`POST /transfer`

    curl -X POST http://localhost:7000/transfer \
      --data @- << EOF
    {
    	"amount": 100.00,
        "targetAccount": "4ce1d1f1-d186-492e-821e-393389f80447",
        "sourceAccount": "64d59250-fb13-40bd-97d0-b3d58d49d835",
        "reference": " a reference for the transfer"
    }
    EOF

### Response

    HTTP/1.1 200 OK
        
    {
      "success" : true,
      "detail" : {
          "transactionId" : "4ce4d1f1-895g-492e-821e-393389f80908",
          "state" : "SUCCESS",
          "completedAt" : "2020-02-12T03:41:33.950Z"
      }
    }  
    
     
## Get a transaction detail

### Request

`GET /transaction`

    curl http://localhost:7000/transaction/ce1d1f1-d186-492e-821e-393389f80447
  

### Response

    HTTP/1.1 200 OK
    
    {
        "transactionId": "87713be9-5589-44f7-9b27-8f7e892c3aa9",
        "state": "SUCCESS",
        "reference": " a reference for the deposit",
        "legs": [
            {
                "legId": "aef8335f-93b0-4771-9ab9-53849b30dbb9",
                "accountId": "30a5e9d1-3a7a-48ee-b45b-26835a4c1243",
                "amount": 100.00
            }
        ],
        "createdAt": "2020-02-12T05:05:48.695Z",
        "updatedAt": "2020-02-12T05:05:48.709Z",
        "completedAt": "2020-02-12T05:05:48.709Z",
    }






