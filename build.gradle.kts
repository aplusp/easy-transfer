import org.gradle.api.tasks.testing.logging.TestLogEvent

version = "1.0-SNAPSHOT"

// Dependencies versions
val exposedVersion = "0.18.1"
val javalinVersion = "3.7.0"
val jacksonVersion = "2.9.7"
val sl4jVersion = "1.7.28"
val unirestVersion = "3.4.03"

plugins {
    // Apply the Kotlin JVM plugin to add support for Kotlin.
    id("org.jetbrains.kotlin.jvm") version "1.3.61"
    id("groovy")

    // Apply the application plugin to add support for building a CLI application.
    application
}

repositories {
    mavenCentral()
    jcenter()
}

dependencies {
    implementation(platform("org.jetbrains.kotlin:kotlin-bom"))
    implementation("org.jetbrains.kotlin:kotlin-stdlib-jdk8")
    implementation("io.javalin:javalin:$javalinVersion")

    // mapping libs
    implementation("com.fasterxml.jackson.core:jackson-databind:$jacksonVersion")
    implementation("com.fasterxml.jackson.datatype:jackson-datatype-joda:$jacksonVersion")
    implementation("com.fasterxml.jackson.module:jackson-module-kotlin:$jacksonVersion")

    // logging
    implementation("org.slf4j:slf4j-api:$sl4jVersion")
    implementation("org.slf4j:slf4j-simple:$sl4jVersion")

    // persistance
    implementation("org.jetbrains.exposed:exposed-core:$exposedVersion")
    implementation("org.jetbrains.exposed:exposed-dao:$exposedVersion")
    implementation("org.jetbrains.exposed:exposed-jdbc:$exposedVersion")
    implementation("org.jetbrains.exposed:exposed-jodatime:$exposedVersion")
    implementation("com.zaxxer:HikariCP:3.4.1")
    implementation("com.h2database:h2:1.4.199")

    // tests
    testApi("org.codehaus.groovy:groovy-all:2.5.8")
    testImplementation("org.spockframework:spock-core:1.2-groovy-2.5")
    testImplementation("org.junit.jupiter:junit-jupiter:5.6.0")
    testImplementation("com.konghq:unirest-java:$unirestVersion")
    testImplementation("com.konghq:unirest-objectmapper-jackson:$unirestVersion")
}

application {
    // Define the main class for the application.
    mainClassName = "io.alessio.easytransfer.AppKt"
}

tasks.withType(Test::class.java) {
    useJUnitPlatform()
    testLogging {
        events.addAll(listOf(TestLogEvent.PASSED, TestLogEvent.FAILED, TestLogEvent.SKIPPED))
        showStandardStreams = false
        showExceptions = false
    }
}

