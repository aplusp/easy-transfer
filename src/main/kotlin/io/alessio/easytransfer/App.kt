package io.alessio.easytransfer

import io.alessio.easytransfer.infrastructure.AppConfig
import org.slf4j.Logger
import org.slf4j.LoggerFactory

val logger: Logger = LoggerFactory.getLogger("app-logger")

fun main() {
    val app = AppConfig().setup().start(7000)
    Runtime.getRuntime().addShutdownHook(Thread {
        logger.info("Shutting down server")
        app.stop()
    })
}
