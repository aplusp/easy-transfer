package io.alessio.easytransfer.application

import io.alessio.easytransfer.commons.Amount
import io.alessio.easytransfer.domain.model.account.Account
import io.alessio.easytransfer.domain.model.account.AccountId
import io.alessio.easytransfer.domain.model.account.AccountRepository
import io.alessio.easytransfer.domain.model.account.AccountStatus
import io.alessio.easytransfer.domain.model.user.User
import io.alessio.easytransfer.persistence.inTransaction

class AccountApplicationService(private val accountRepository: AccountRepository) {

    /**
     * Create a new account setting it to active for sake of semplicity
     */
    fun createNewAccountForUser(user: User): Account {
        return inTransaction {

            val account = Account(
                    id = accountRepository.nextIdentity(),
                    state = AccountStatus.ACTIVE,
                    currentBalance = Amount.of(0),
                    linkedUser = user.id
            )
            accountRepository.create(account)
        }
    }

    /**
     * Returns the account identified by [accountId]
     */
    fun getAccount(accountId: AccountId): Account = accountRepository.getById(accountId)

}
