package io.alessio.easytransfer.application

import io.alessio.easytransfer.commons.Amount
import io.alessio.easytransfer.domain.model.account.AccountRepository
import io.alessio.easytransfer.domain.model.account.NotEnoughFundsException
import io.alessio.easytransfer.domain.model.transaction.DepositCommand
import io.alessio.easytransfer.domain.model.transaction.Transaction
import io.alessio.easytransfer.domain.model.transaction.TransactionFailures
import io.alessio.easytransfer.domain.model.transaction.TransactionId
import io.alessio.easytransfer.domain.model.transaction.TransactionRepository
import io.alessio.easytransfer.domain.model.transaction.TransactionableCommand
import io.alessio.easytransfer.domain.model.transaction.TransferCommand
import io.alessio.easytransfer.persistence.inSerializableTransaction
import io.alessio.easytransfer.utils.Preconditions
import org.slf4j.Logger
import org.slf4j.LoggerFactory


class TransactionApplicationService(
        private val transactionRespository: TransactionRepository,
        private val accountRepository: AccountRepository
) {
    companion object {
        val logger: Logger = LoggerFactory.getLogger(TransactionApplicationService::class.java)
    }

    fun transfer(command: TransferCommand): Transaction {
        Preconditions.assert({ command.sourceAccount.id != command.targetAccount.id }, "Cannot transfer money between same account")
        Preconditions.assert({ command.amount > Amount.ZERO }, "Cannot transfer 0 or negative amount")

        return doTransferWithTransaction(command) {
            val fromAccount = accountRepository.getById(command.sourceAccount)
            val toAccount = accountRepository.getById(command.targetAccount)
            fromAccount.whitdraw(command.amount)
            toAccount.deposit(command.amount)
            accountRepository.update(fromAccount)
            accountRepository.update(toAccount)

        }
    }

    fun deposit(depositCommand: DepositCommand): Transaction {
        return doTransferWithTransaction(depositCommand) {
            val account = accountRepository.getById(depositCommand.targetAccount)
            account.deposit(depositCommand.amount)
            accountRepository.update(account)
        }
    }

    fun getTransaction(transactionId: TransactionId): Transaction {
        return transactionRespository.getById(transactionId)
    }

    /**
     * Excutes the [statement] lambda handling all the [Transaction] generation aspects
     */
    private fun doTransferWithTransaction(command: TransactionableCommand, statement: Transaction.() -> Unit): Transaction {
        val transaction: Transaction = when (command) {
            is DepositCommand -> Transaction.buildForDeposit(transactionRespository.nextIdentity(), command)
            is TransferCommand -> Transaction.buildForTransfer(transactionRespository.nextIdentity(), command)
            else -> throw IllegalArgumentException()
        }
        // Store new transaction in separate db-transaction so we can store if failed or succeeded
        val pendingTransaction = transactionRespository.create(transaction)

        // Do the actual transfer inside a Serializable transaction
        return try {
            inSerializableTransaction {
                try {
                    statement.invoke(transaction)
                    transactionRespository.update(pendingTransaction.markAscompleted())
                } catch (ex: Exception) {
                    rollback()
                    throw ex
                }
            }
        } catch (ex: NotEnoughFundsException) {
            logger.error("${pendingTransaction.transactionId} declined: ${ex.javaClass.name}")
            transactionRespository.update(pendingTransaction.markAsDeclined())
        } catch (ex: Exception) {
            logger.error("${pendingTransaction.transactionId} failed: ${ex.message ?: ex.javaClass.name}")
            transactionRespository.update(pendingTransaction.markAsFailed(TransactionFailures.mapFromException(ex).name))
        }
    }
}
