package io.alessio.easytransfer.application

import io.alessio.easytransfer.domain.model.user.CreateUserCommand
import io.alessio.easytransfer.domain.model.user.User
import io.alessio.easytransfer.domain.model.user.UserId
import io.alessio.easytransfer.domain.model.user.UserRepository
import io.alessio.easytransfer.persistence.inTransaction
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import java.lang.RuntimeException

class UserAlreadyExistsException(override val message: String) : RuntimeException(message) {
    constructor() : this("User already exists")
}

class UserApplicationService(private val userRepository: UserRepository,
                             private val accountApplicationService: AccountApplicationService) {

    companion object {
        val logger: Logger = LoggerFactory.getLogger(UserApplicationService::class.java)
    }

    /**
     * Create a new [User] checking if an user with the given email address already exists.
     * Upon the user creation an account is created and attached to the user
     * TODO: Re-think about haveing a dependency on the account application service.
     */
    fun createUser(createAccountCommand: CreateUserCommand): User {
        return inTransaction {
            if (userRepository.existsByEmail(createAccountCommand.email)) {
                throw UserAlreadyExistsException("User with email ${createAccountCommand.email} already exists")
            }
            val user = userRepository.create(User(
                    id = userRepository.nextIdentity(),
                    firstName = createAccountCommand.firstName,
                    lastName = createAccountCommand.lastName,
                    email = createAccountCommand.email,
                    phoneNumber = createAccountCommand.phoneNumber,
                    accounts = listOf()
            ))
            val account = accountApplicationService.createNewAccountForUser(user)
            user.registerAccount(account)
        }
    }

    fun getUser(userId: UserId): User = userRepository.getById(userId)
}