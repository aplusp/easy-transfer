package io.alessio.easytransfer.commons

import java.math.BigDecimal
import java.math.RoundingMode
import java.util.*

class Amount private constructor(val value: BigDecimal) {

    companion object {
        private val AMOUNT_FORMAT: Regex = Regex("^([0-9]+)\$|([0-9]+)?\\.([0-9]+)\$")
        private const val SCALE: Int = 2
        private val ROUNDING_MODE = RoundingMode.HALF_EVEN
        val ZERO = of(0)

        @JvmStatic
        fun of(decimals: Long, cents: Long = 0L): Amount {
            return Amount(BigDecimal("$decimals.$cents")
                    .setScale(SCALE, ROUNDING_MODE))
        }

        @JvmStatic
        fun fromString(stringValue: String): Amount {
            require(stringValue.isNotBlank())
            val matchResult = AMOUNT_FORMAT.find(stringValue)
                    ?: throw IllegalArgumentException("non parsable amount $stringValue")
            val (fullMatch, decimals, fractional) = matchResult.destructured
            if (fullMatch.isNotBlank()) {
                return of(fullMatch.toLong())
            }
            return of(decimals.toLong(), fractional.toLong())
        }

        @JvmStatic
        fun fromBigDecimal(bigDecimalValue: BigDecimal): Amount {
            Objects.requireNonNull(bigDecimalValue)
            return Amount(bigDecimalValue.setScale(SCALE, ROUNDING_MODE))
        }
    }

    fun toBigDecimal(): BigDecimal {
        return this.value
    }

    val positiveValue: Amount
        get() = fromBigDecimal(this.value.abs())

    val negativeValue: Amount
        get() = fromBigDecimal(this.value.abs().negate())


    override fun equals(other: Any?): Boolean {
        if (other == null || other !is Amount) return false
        return value.compareTo(other.value) == 0
    }

    override fun toString(): String {
        return value.toPlainString()
    }

    override fun hashCode(): Int {
        return value.hashCode()
    }

    operator fun compareTo(amount: Amount): Int {
        return value.compareTo(amount.value)
    }
}

operator fun Amount.minus(other: Amount): Amount = Amount.fromBigDecimal(this.value.subtract(other.value))
operator fun Amount.plus(other: Amount): Amount = Amount.fromBigDecimal(this.value.add(other.value))
