package io.alessio.easytransfer.commons

interface IdentifiedBy<T> { val id: T }