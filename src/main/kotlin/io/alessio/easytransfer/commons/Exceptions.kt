package io.alessio.easytransfer.commons

import java.lang.RuntimeException

open class EntityNotFoundException(message: String): RuntimeException(message)