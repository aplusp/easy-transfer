package io.alessio.easytransfer.commons

interface IdentityRepository<T> {
    fun nextIdentity(): T
}