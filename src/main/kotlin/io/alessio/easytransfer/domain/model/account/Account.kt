package io.alessio.easytransfer.domain.model.account

import io.alessio.easytransfer.commons.Amount
import io.alessio.easytransfer.commons.IdentifiedBy
import io.alessio.easytransfer.commons.minus
import io.alessio.easytransfer.commons.plus
import io.alessio.easytransfer.domain.model.account.AccountStatus.ACTIVE
import io.alessio.easytransfer.domain.model.account.AccountStatus.CREATED
import io.alessio.easytransfer.domain.model.account.AccountStatus.SUSPENDED
import io.alessio.easytransfer.domain.model.user.UserId
import org.joda.time.DateTime
import java.lang.IllegalStateException
import java.util.*

class NotEnoughFundsException(message: String? = null) : RuntimeException(message)
class InactiveAccountException(message: String? = null) : RuntimeException(message)
data class AccountId(val id: UUID)


/**
 * A class representing a bank account based on following assumption.
 * An non-active account cannot send not receive money
 * An account can not have a negative balance.
 * An account is linked at most to one user
 * An account's user cannot be changed
 */
class Account(override val id: AccountId,
              var currentBalance: Amount,
              var state: AccountStatus = CREATED,
              private var linkedUser: UserId? = null) : IdentifiedBy<AccountId> {

    lateinit var createdAt: DateTime
    lateinit var updateAt: DateTime

    fun linkedUser() = linkedUser

    fun activate() {
        state = ACTIVE
    }

    fun disable() {
        state = SUSPENDED
    }

    fun whitdraw(amount: Amount) {
        assertActive()
        if (this.hasEnoughFunds(amount)) {
            currentBalance = currentBalance.minus(amount)
        } else {
            throw NotEnoughFundsException()
        }
    }

    fun deposit(amount: Amount) {
        assertActive()
        currentBalance = currentBalance.plus(amount)
    }

    fun linkUser(userId: UserId) {
        if (linkedUser != null) {
            throw IllegalStateException("Account is already linked to an user with id $linkedUser")
        }
        linkedUser = userId
    }

    private fun hasEnoughFunds(amount: Amount): Boolean {
        return currentBalance > amount
    }

    private fun assertActive() {
        if (state != ACTIVE) throw InactiveAccountException()
    }

}
