package io.alessio.easytransfer.domain.model.account

import io.alessio.easytransfer.commons.EntityNotFoundException
import io.alessio.easytransfer.commons.IdentityRepository

interface AccountRepository: IdentityRepository<AccountId> {

    fun create(account: Account): Account

    /**
     * Retrieves an [Account] by its [AccountId]
     */
    @Throws(EntityNotFoundException::class)
    fun getById(accountId: AccountId): Account

    /**
     * updates the given [Account]
     */
    fun update(account: Account)
}