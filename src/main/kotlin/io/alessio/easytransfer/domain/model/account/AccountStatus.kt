package io.alessio.easytransfer.domain.model.account

enum class AccountStatus(val code: String) {
    CREATED("CREATED"),
    SUSPENDED("SUSPENDED"),
    ACTIVE("ACTIVE")
}