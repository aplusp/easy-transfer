package io.alessio.easytransfer.domain.model.transaction

import io.alessio.easytransfer.commons.Amount
import io.alessio.easytransfer.domain.model.account.AccountId
import org.joda.time.DateTime
import java.util.*

enum class TransactionState {
    PENDING,
    SUCCESS,
    FAILED,
    DECLINED
}

data class TransactionId(val identifier: UUID)
data class TransactionLegId(val identifier: UUID)

data class TransactionLeg(val legId: TransactionLegId? = null,
                          val amount: Amount,
                          val accountId: AccountId)

data class Transaction constructor(
        val transactionId: TransactionId,
        val state: TransactionState,
        val reference: String,
        val legs: Collection<TransactionLeg> = listOf(),
        var createdAt: DateTime,
        var updatedAt: DateTime,
        var completedAt: DateTime? = null,
        var failureReason: String? = null
) {

    fun markAscompleted(): Transaction {
        val now = DateTime.now()
        return copy(state = TransactionState.SUCCESS, completedAt = now, updatedAt = now)
    }

    fun markAsFailed(reason: String): Transaction = copy(state = TransactionState.FAILED, failureReason = reason)

    fun markAsDeclined(): Transaction = copy(state = TransactionState.DECLINED,
            failureReason = TransactionFailures.NOT_ENOUGH_FUNDS.name)

    fun isSuccessfull(): Boolean = state == TransactionState.SUCCESS

    companion object {
        fun buildForTransfer(transactionId: TransactionId, transferCommand: TransferCommand): Transaction {
            return withLegs(transactionId, transferCommand.reference, listOf(
                    TransactionLeg(amount = transferCommand.amount.negativeValue, accountId = transferCommand.sourceAccount),
                    TransactionLeg(amount = transferCommand.amount.positiveValue, accountId = transferCommand.targetAccount)
            ))
        }

        fun buildForDeposit(transactionId: TransactionId, depositCommand: DepositCommand): Transaction {
            return withLegs(transactionId,
                    depositCommand.reference,
                    listOf(TransactionLeg(amount = depositCommand.amount, accountId = depositCommand.targetAccount)))
        }

        private fun withLegs(transactionId: TransactionId,
                             reference: String,
                             legs: List<TransactionLeg>): Transaction {
            val now = DateTime.now()
            return Transaction(
                    transactionId = transactionId,
                    reference = reference,
                    updatedAt = now,
                    createdAt = now,
                    state = TransactionState.PENDING,
                    legs = legs
            )
        }
    }
}
