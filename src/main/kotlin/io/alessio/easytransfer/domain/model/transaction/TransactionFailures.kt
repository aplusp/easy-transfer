package io.alessio.easytransfer.domain.model.transaction

import io.alessio.easytransfer.commons.EntityNotFoundException
import io.alessio.easytransfer.domain.model.account.InactiveAccountException
import io.alessio.easytransfer.domain.model.account.NotEnoughFundsException

enum class TransactionFailures {
    NOT_ENOUGH_FUNDS,
    SYSTEM_FAILURE,
    INACTIVE_ACCOUNT,
    UNKNOWN_ACCOUNT;

    companion object {
        fun mapFromException(ex: Exception): TransactionFailures {
            return when (ex) {
                is NotEnoughFundsException -> NOT_ENOUGH_FUNDS
                is EntityNotFoundException -> UNKNOWN_ACCOUNT
                is InactiveAccountException -> INACTIVE_ACCOUNT
                else -> SYSTEM_FAILURE
            }
        }
    }
}

