package io.alessio.easytransfer.domain.model.transaction

import io.alessio.easytransfer.commons.IdentityRepository
import io.alessio.easytransfer.domain.model.account.AccountId

interface TransactionRepository: IdentityRepository<TransactionId> {

    fun findTransactionsForAccount(accountId: AccountId): Collection<Transaction>

    fun update(transaction: Transaction): Transaction

    fun create(transaction: Transaction): Transaction

    fun getById(transactionId: TransactionId): Transaction
}