package io.alessio.easytransfer.domain.model.transaction

import io.alessio.easytransfer.commons.Amount
import io.alessio.easytransfer.domain.model.account.AccountId

interface TransactionableCommand {
    /**
     * the account the money will be sent to
     */
    val targetAccount: AccountId
    /**
     * the amount
     */
    val amount: Amount
    /**
     * the reference for the transaction
     */
    val reference: String
}