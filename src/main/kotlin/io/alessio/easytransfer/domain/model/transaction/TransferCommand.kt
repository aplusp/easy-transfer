package io.alessio.easytransfer.domain.model.transaction

import io.alessio.easytransfer.commons.Amount
import io.alessio.easytransfer.domain.model.account.AccountId

data class TransferCommand(
        /**
         * the account the money will be sent from
         */
        val sourceAccount: AccountId,
        override val targetAccount: AccountId,
        override val amount: Amount,
        override val reference: String
): TransactionableCommand