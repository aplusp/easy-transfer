package io.alessio.easytransfer.domain.model.user

/**
 * A command for the creation of a *new* user
 */
data class CreateUserCommand(
        val email: String,
        val firstName: String,
        val lastName: String,
        val phoneNumber: String
)