package io.alessio.easytransfer.domain.model.user

import io.alessio.easytransfer.commons.IdentifiedBy
import io.alessio.easytransfer.domain.model.account.Account
import io.alessio.easytransfer.domain.model.account.AccountId
import org.joda.time.DateTime
import java.util.*

data class UserId(val id: UUID)

/**
 * A User is a physical entity who can have one or more banking accounts
 */
data class User(override val id: UserId,
                val email: String,
                val firstName: String,
                val lastName: String,
                val phoneNumber: String,
                val accounts: List<AccountId> = listOf(),
                val createdAt: DateTime = DateTime.now()) : IdentifiedBy<UserId> {

    fun registerAccount(account: Account) = copy(accounts = accounts + listOfNotNull(account.id))

}