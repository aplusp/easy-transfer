package io.alessio.easytransfer.domain.model.user

import io.alessio.easytransfer.commons.IdentityRepository

interface UserRepository: IdentityRepository<UserId> {

    fun create(user: User): User

    fun getById(userId: UserId): User
    fun existsByEmail(email: String): Boolean
}