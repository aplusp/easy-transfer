package io.alessio.easytransfer.infrastructure

import io.alessio.easytransfer.infrastructure.web.api.v1.ErrorExceptionMapping
import io.alessio.easytransfer.infrastructure.web.api.v1.Router
import io.alessio.easytransfer.infrastructure.web.api.v1.controllers.AccountController
import io.alessio.easytransfer.infrastructure.web.api.v1.controllers.TransactionController
import io.alessio.easytransfer.infrastructure.web.api.v1.controllers.UserController
import io.alessio.easytransfer.persistence.DatabaseInitializer
import io.alessio.easytransfer.persistence.EasyTransferDatabase
import io.javalin.Javalin
import io.javalin.plugin.json.JavalinJackson
import org.slf4j.Logger
import org.slf4j.LoggerFactory

val appLogger: Logger = LoggerFactory.getLogger("easy-transfer")

class AppConfig {
    fun setup(dependencyManager: DependencyManager = DeafultDependencyManager()): Javalin {
        val databaseFactory = dependencyManager.databaseFactory
        // Initialize the database
        if (databaseFactory is DatabaseInitializer) {
            EasyTransferDatabase.init(databaseFactory)
            databaseFactory.initializeSchemas()
        }
        return Javalin.create {
            JavalinJackson.configure(dependencyManager.objectMapper)
            // Initialize a very basic request logger
            it.requestLogger { ctx, ms ->
                appLogger.info("${ctx.req.method} ${ctx.req.requestURI} -> ${ctx.res.status} ($ms msec)")
            }
        }.also { app ->
            val router = Router(
                    accountController = AccountController(dependencyManager.accountApplicationService),
                    userController = UserController(dependencyManager.userApplicationService),
                    transactionController = TransactionController(dependencyManager.transactionApplicationService)
            )
            router.registerRoutes(app)
            ErrorExceptionMapping.register(app)
        }

    }
}