package io.alessio.easytransfer.infrastructure

import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.databind.SerializationFeature
import com.fasterxml.jackson.datatype.joda.JodaModule
import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import io.alessio.easytransfer.application.AccountApplicationService
import io.alessio.easytransfer.application.TransactionApplicationService
import io.alessio.easytransfer.application.UserApplicationService
import io.alessio.easytransfer.domain.model.account.AccountRepository
import io.alessio.easytransfer.domain.model.transaction.TransactionRepository
import io.alessio.easytransfer.domain.model.user.UserRepository
import io.alessio.easytransfer.persistence.DatabaseFactory
import io.alessio.easytransfer.persistence.DatasourceDatabaseFactory
import io.alessio.easytransfer.persistence.SqlAccountRepository
import io.alessio.easytransfer.persistence.SqlTransactionRepository
import io.alessio.easytransfer.persistence.SqlUserRepository
import java.text.SimpleDateFormat


interface DependencyManager {
    val accountRepository: AccountRepository
    val userRepository: UserRepository
    val transactionRepository: TransactionRepository
    val accountApplicationService: AccountApplicationService
    val userApplicationService: UserApplicationService
    val transactionApplicationService: TransactionApplicationService
    val databaseFactory: DatabaseFactory
    val objectMapper: ObjectMapper
}

public class DeafultDependencyManager : DependencyManager {

    companion object {
        private var accountRepositoryInternal: AccountRepository? = null
        private var userRepositoryInternal: UserRepository? = null
        private var transactionRepositoryInternal: TransactionRepository? = null
        private var accountApplicationServiceInternal: AccountApplicationService? = null
        private var userApplicationServiceInternal: UserApplicationService? = null
        private var transactionApplicationServiceInternal: TransactionApplicationService? = null
        private var databaseFactoryInternal: DatabaseFactory? = null
        private var objectMapperInternal: ObjectMapper? = null
    }

    override val accountRepository: AccountRepository
        get() {
            if (accountRepositoryInternal == null) {
                synchronized(this) {
                    accountRepositoryInternal = SqlAccountRepository()
                }
            }
            return accountRepositoryInternal!!
        }
    override val userRepository: UserRepository
        get() {
            if (userRepositoryInternal == null) {
                synchronized(this) {
                    userRepositoryInternal = SqlUserRepository()
                }
            }
            return userRepositoryInternal!!
        }

    override val transactionRepository: TransactionRepository
        get() {
            if (transactionRepositoryInternal == null) {
                synchronized(this) {
                    transactionRepositoryInternal = SqlTransactionRepository()
                }
            }
            return transactionRepositoryInternal!!
        }

    override val accountApplicationService: AccountApplicationService
        get() {
            if (accountApplicationServiceInternal == null) {
                synchronized(this) {
                    accountApplicationServiceInternal = AccountApplicationService(accountRepository = accountRepository)
                }
            }
            return accountApplicationServiceInternal!!
        }

    override val userApplicationService: UserApplicationService
        get() {
            if (userApplicationServiceInternal == null) {
                synchronized(this) {
                    userApplicationServiceInternal = UserApplicationService(
                            userRepository = userRepository,
                            accountApplicationService = accountApplicationService)
                }
            }
            return userApplicationServiceInternal!!
        }

    override val transactionApplicationService: TransactionApplicationService
        get() {
            if (transactionApplicationServiceInternal == null) {
                synchronized(this) {
                    transactionApplicationServiceInternal = TransactionApplicationService(
                            transactionRepository,
                            accountRepository
                    )
                }
            }
            return transactionApplicationServiceInternal!!
        }

    override val databaseFactory: DatabaseFactory
        get() {
            if (databaseFactoryInternal == null) {
                synchronized(this) {
                    databaseFactoryInternal = DatasourceDatabaseFactory()
                }
            }
            return databaseFactoryInternal!!
        }

    override val objectMapper: ObjectMapper
        get() {
            if (objectMapperInternal == null) {
                synchronized(this) {
                    objectMapperInternal = jacksonObjectMapper()
                            .disable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS)
                            .setDateFormat(SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"))
                            .registerModule(JodaModule())
                }
            }
            return objectMapperInternal!!
        }
}