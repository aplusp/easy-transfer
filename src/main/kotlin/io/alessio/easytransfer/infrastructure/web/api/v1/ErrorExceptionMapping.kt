package io.alessio.easytransfer.infrastructure.web.api.v1

import io.alessio.easytransfer.application.UserAlreadyExistsException
import io.alessio.easytransfer.commons.EntityNotFoundException
import io.javalin.Javalin
import io.javalin.http.BadRequestResponse
import io.javalin.http.HttpResponseException
import io.javalin.http.NotFoundResponse
import org.eclipse.jetty.http.HttpStatus
import org.jetbrains.exposed.exceptions.ExposedSQLException
import org.slf4j.LoggerFactory

internal data class ErrorResponse(val errors: Map<String, List<String?>>)

object ErrorExceptionMapping {
    private val LOG = LoggerFactory.getLogger(ErrorExceptionMapping::class.java)

    fun register(app: Javalin) {
        app.exception(Exception::class.java) { e, ctx ->
            LOG.error("Exception occurred for req -> ${ctx.url()}", e)
            val error = ErrorResponse(mapOf("Internal Error" to listOf(e.message ?: "Error occurred!")))
            ctx.json(error).status(HttpStatus.INTERNAL_SERVER_ERROR_500)
        }
        app.exception(ExposedSQLException::class.java) { e, ctx ->
            LOG.error("Exception occurred for req -> ${ctx.url()}", e)
            val error = ErrorResponse(mapOf("Unknow Error" to listOf("Error occurred!")))
            ctx.json(error).status(HttpStatus.INTERNAL_SERVER_ERROR_500)
        }

        app.exception(BadRequestResponse::class.java) { e, ctx ->
            LOG.warn("BadRequestResponse occurred for req -> ${ctx.url()}")
            val error = ErrorResponse(mapOf("body" to listOf("Invalid request: ${e.message}")))
            ctx.json(error).status(HttpStatus.BAD_REQUEST_400)
        }
        app.exception(UserAlreadyExistsException::class.java) { e, ctx ->
            LOG.warn("UserAlreadyExistsException occurred for req -> ${ctx.url()}")
            val error = ErrorResponse(mapOf("body" to listOf(e.message)))
            ctx.json(error).status(HttpStatus.BAD_REQUEST_400)
        }
        app.exception(IllegalArgumentException::class.java) { e, ctx ->
            LOG.warn("BadRequestResponse occurred for req -> ${ctx.url()}")
            val error = ErrorResponse(mapOf("body" to listOf("${e.message}")))
            ctx.json(error).status(HttpStatus.BAD_REQUEST_400)
        }

        app.exception(NotFoundResponse::class.java) { _, ctx ->
            LOG.warn("NotFoundResponse occurred for req -> ${ctx.url()}")
            val error = ErrorResponse(mapOf("body" to listOf("Resource can't be found to fulfill the request.")))
            ctx.json(error).status(HttpStatus.NOT_FOUND_404)
        }

        app.exception(EntityNotFoundException::class.java) { e, ctx ->
            LOG.warn("EntityNotFoundException occurred for req -> ${ctx.url()}")
            val error = ErrorResponse(mapOf("body" to listOf("${e.message}")))
            ctx.json(error).status(HttpStatus.NOT_FOUND_404)
        }

        app.exception(HttpResponseException::class.java) { e, ctx ->
            LOG.warn("HttpResponseException occurred for req -> ${ctx.url()}")
            val error = ErrorResponse(mapOf("body" to listOf(e.message)))
            ctx.json(error).status(e.status)
        }
    }
}