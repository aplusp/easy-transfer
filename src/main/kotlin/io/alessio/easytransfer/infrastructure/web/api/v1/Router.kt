package io.alessio.easytransfer.infrastructure.web.api.v1

import io.alessio.easytransfer.infrastructure.web.api.v1.controllers.AccountController
import io.alessio.easytransfer.infrastructure.web.api.v1.controllers.TransactionController
import io.alessio.easytransfer.infrastructure.web.api.v1.controllers.UserController
import io.javalin.Javalin
import io.javalin.apibuilder.ApiBuilder.get
import io.javalin.apibuilder.ApiBuilder.path
import io.javalin.apibuilder.ApiBuilder.post

/**
 * RESTful api router
 */
class Router(
        private val accountController: AccountController,
        private val userController: UserController,
        private val transactionController: TransactionController
) {

    /**
     * Registers the routes for the easy transfer API
     */
    fun registerRoutes(app: Javalin) {
        app.routes {
            path("account") {
                get(":id", accountController::get)
            }
            path("user") {
                get(":id", userController::get)
                post(userController::post)
            }
            path("transfer") {
                post(transactionController::transfer)
            }

            path("pay") {
                post(transactionController::deposit)
            }

            path("transaction") {
                get(":id", transactionController::get)
            }
        }
    }
}