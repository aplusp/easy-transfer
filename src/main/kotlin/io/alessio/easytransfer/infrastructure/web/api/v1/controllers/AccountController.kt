package io.alessio.easytransfer.infrastructure.web.api.v1.controllers

import io.alessio.easytransfer.application.AccountApplicationService
import io.alessio.easytransfer.domain.model.account.Account
import io.alessio.easytransfer.domain.model.account.AccountId
import io.alessio.easytransfer.domain.model.account.AccountStatus
import io.alessio.easytransfer.utils.isUUID
import io.javalin.http.Context
import org.joda.time.DateTime
import java.math.BigDecimal
import java.util.*

data class AccountDto(
        val accountId: String,
        val currentBalance: BigDecimal,
        val state: AccountStatus,
        val createdAt: DateTime,
        val updatedAt: DateTime
)

/**
 * RESTful endpoint for [Account]s related operations
 */
class AccountController(private val accountApplicationService: AccountApplicationService) {

    fun get(ctx: Context) {
        ctx.pathParam<String>("id").check({ it.isNotEmpty() && it.isUUID() }).get().apply {
            accountApplicationService.getAccount(AccountId(UUID.fromString(this))).toTransactionDto().run {
                ctx.json(this).status(200)
            }
        }
    }
}

private fun Account.toTransactionDto(): AccountDto {
    return AccountDto(
            accountId = id.id.toString(),
            currentBalance = currentBalance.value,
            state = state,
            createdAt = createdAt,
            updatedAt = updateAt
    )
}

