package io.alessio.easytransfer.infrastructure.web.api.v1.controllers

import io.alessio.easytransfer.application.TransactionApplicationService
import io.alessio.easytransfer.commons.Amount
import io.alessio.easytransfer.domain.model.account.AccountId
import io.alessio.easytransfer.domain.model.transaction.DepositCommand
import io.alessio.easytransfer.domain.model.transaction.Transaction
import io.alessio.easytransfer.domain.model.transaction.TransactionId
import io.alessio.easytransfer.domain.model.transaction.TransactionState
import io.alessio.easytransfer.domain.model.transaction.TransferCommand
import io.alessio.easytransfer.utils.isUUID
import io.javalin.http.Context
import org.joda.time.DateTime
import java.math.BigDecimal
import java.util.*

data class TransactionLegDto(val legId: String,
                             val accountId: String,
                             val amount: BigDecimal)

data class TransactionDto(
        val transactionId: String,
        val state: TransactionState,
        val reference: String,
        val legs: List<TransactionLegDto>,
        var createdAt: DateTime,
        var updatedAt: DateTime,
        var completedAt: DateTime? = null,
        var failureReason: String? = null
)

data class TransferCommandDto(
        val amount: BigDecimal,
        val sourceAccount: String,
        val targetAccount: String,
        val reference: String
)

data class DepositCommandDto(
        val amount: BigDecimal,
        val targetAccount: String,
        val reference: String
)

data class TransactionResponseDto(
        val success: Boolean,
        val failureReason: String,
        val detail: TransactionResponseDetailDto
)

data class TransactionResponseDetailDto(val transactionId: String,
                                        val state: TransactionState,
                                        val completedAt: DateTime?)

/**
 * RESTful endpoint for transactions related operations
 */
class TransactionController(private val transactionApplicationService: TransactionApplicationService) {

    fun get(ctx: Context) {
        ctx.pathParam<String>("id").check({ it.isNotEmpty() && it.isUUID() }).get().apply {
            transactionApplicationService.getTransaction(TransactionId(UUID.fromString(this)))
                    .toTransactionDto().also { ctx.json(it).status(200) }
        }
    }

    fun transfer(ctx: Context) {
        ctx.bodyValidator(TransferCommandDto::class.java)
                .check({ it.amount > BigDecimal.ZERO }, errorMessage = "Cannot transfer negative amount")
                .check({ it.sourceAccount.isNotEmpty() && it.sourceAccount.isUUID() })
                .check({ it.targetAccount.isNotEmpty() && it.targetAccount.isUUID() })
                .get().also {
                    transactionApplicationService.transfer(it.toTransferCommand()).toTransactionResponseDto().send(ctx)
                }
    }

    fun deposit(ctx: Context) {
        ctx.bodyValidator(DepositCommandDto::class.java)
                .check({ it.amount > BigDecimal.ZERO }, errorMessage = "Cannot deposit negative amount")
                .check({ it.targetAccount.isNotEmpty() && it.targetAccount.isUUID() }).get().also {
                    transactionApplicationService.deposit(it.toDepositCommand()).toTransactionResponseDto().send(ctx)
                }
    }

}

private fun TransactionResponseDto.send(ctx: Context) = ctx.json(this).status(if (this.success) 200 else 409)

private fun Transaction.toTransactionResponseDto(): TransactionResponseDto {
    return TransactionResponseDto(
            success = isSuccessfull(),
            failureReason = failureReason.orEmpty(),
            detail = TransactionResponseDetailDto(
                    transactionId = transactionId.identifier.toString(),
                    state = state,
                    completedAt = completedAt
            )
    )
}

private fun TransferCommandDto.toTransferCommand(): TransferCommand {
    return TransferCommand(
            sourceAccount = AccountId(UUID.fromString(sourceAccount)),
            targetAccount = AccountId(UUID.fromString(targetAccount)),
            amount = Amount.fromBigDecimal(amount),
            reference = reference
    )
}

private fun DepositCommandDto.toDepositCommand(): DepositCommand {
    return DepositCommand(
            targetAccount = AccountId(UUID.fromString(targetAccount)),
            amount = Amount.fromBigDecimal(amount),
            reference = reference
    )
}

private fun Transaction.toTransactionDto(): TransactionDto {
    return TransactionDto(transactionId.identifier.toString(),
            state,
            reference,
            legs.map {
                TransactionLegDto(it.legId!!.identifier.toString(),
                        it.accountId.id.toString(),
                        it.amount.value)
            },
            createdAt,
            updatedAt,
            completedAt,
            failureReason)
}
