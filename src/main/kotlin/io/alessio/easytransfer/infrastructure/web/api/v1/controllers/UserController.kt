package io.alessio.easytransfer.infrastructure.web.api.v1.controllers

import io.alessio.easytransfer.application.UserApplicationService
import io.alessio.easytransfer.domain.model.user.CreateUserCommand
import io.alessio.easytransfer.domain.model.user.User
import io.alessio.easytransfer.domain.model.user.UserId
import io.alessio.easytransfer.utils.isEmailValid
import io.javalin.http.Context
import org.joda.time.DateTime
import java.util.*

data class UserDto(val userId: String,
                   val firstName: String,
                   val lastName: String,
                   val emailAddress: String,
                   val phoneNumber: String,
                   val createdOn: DateTime,
                   val accounts: List<AccountRefDto>)

data class AccountRefDto(val accountId: String)

/**
 * RESTful endpoint for [User] related operations
 */
class UserController(private val userApplicationService: UserApplicationService) {

    fun get(ctx: Context) {
        ctx.pathParam<String>("id").check({ it.isNotEmpty() }).get().apply {
            userApplicationService.getUser(UserId(UUID.fromString(this))).toUserDto()
                    .run { ctx.json(this).status(200) }
        }
    }

    fun post(ctx: Context) {
        ctx.bodyValidator<CreateUserCommand>()
                .check({ it.email.isNotBlank() })
                .check({ it.email.isEmailValid() }, "Email not valid")
                .check({ it.firstName.isNotBlank() })
                .check({ it.lastName.isNotBlank() })
                .check({ it.phoneNumber.isNotBlank() })
                .get().run {
                    userApplicationService.createUser(this).toUserDto()
                            .run { ctx.json(this).status(201) }
                }
    }
}

private fun User.toUserDto(): UserDto {
    return UserDto(this.id.id.toString(),
            this.firstName,
            this.lastName,
            this.email,
            this.phoneNumber,
            this.createdAt,
            this.accounts.map {
                AccountRefDto(it.id.toString())
            }
    )
}
