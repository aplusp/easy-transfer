package io.alessio.easytransfer.persistence

import com.zaxxer.hikari.HikariConfig
import com.zaxxer.hikari.HikariDataSource
import org.jetbrains.exposed.sql.Database
import org.jetbrains.exposed.sql.SchemaUtils
import org.jetbrains.exposed.sql.Transaction
import org.jetbrains.exposed.sql.transactions.DEFAULT_ISOLATION_LEVEL
import org.jetbrains.exposed.sql.transactions.ThreadLocalTransactionManager
import org.jetbrains.exposed.sql.transactions.transaction
import java.sql.Connection
import java.util.concurrent.atomic.AtomicReference
import javax.sql.DataSource

const val DEFAULT_DATBASE_URL = "jdbc:h2:mem:easytransfers"
const val DEFAULT_DATBASE_DRIVER = "org.h2.Driver"

interface DatabaseFactory {
    fun connect(): Database
}

interface DatabaseInitializer {
    fun initializeSchemas()
}

abstract class BaseDatabaseFactory : DatabaseFactory, DatabaseInitializer {
    override fun initializeSchemas() {
        inTransaction {
            SchemaUtils.create(AccountTable,
                    TransactionTable,
                    TransactionLegTable,
                    UserTable)
        }
    }

    abstract override fun connect(): Database

}

class DefaultDatabaseFactory(val databaseUrl: String = "${DEFAULT_DATBASE_URL};DB_CLOSE_DELAY=-1") : BaseDatabaseFactory() {
    override fun connect(): Database {
        return Database.connect(
                url = databaseUrl,
                driver = DEFAULT_DATBASE_DRIVER,
                manager = { ThreadLocalTransactionManager(it, DEFAULT_ISOLATION_LEVEL, 1) }
        )
    }

}

class DatasourceDatabaseFactory : BaseDatabaseFactory() {
    override fun connect(): Database {
        return Database.connect(dataSource)
    }

    companion object {
        val dataSource: DataSource by lazy {
            hikari()
        }

        private fun hikari(): HikariDataSource {
            val config = HikariConfig().also { config ->
                config.driverClassName = DEFAULT_DATBASE_DRIVER
                config.jdbcUrl = "jdbc:h2:mem:easytransfers"
                config.maximumPoolSize = 10
                config.isAutoCommit = false
                config.transactionIsolation = "TRANSACTION_REPEATABLE_READ"
                config.validate()
            }
            return HikariDataSource(config)
        }
    }
}

class EasyTransferDatabase {

    companion object {

        private val factoryHolder: AtomicReference<DatabaseFactory> = AtomicReference()

        fun init(databaseFactory: DatabaseFactory) = factoryHolder.set(databaseFactory)

        fun init(databaseFactorySupplier: () -> DatabaseFactory) = factoryHolder.set(databaseFactorySupplier())

        val db: Database
            get() {
                val databaseFactory = factoryHolder.get()
                        ?: throw IllegalStateException("Please initialize easy-transfer database before")
                return databaseFactory.connect()
            }
    }
}

fun <T> inTransaction(statement: Transaction.() -> T): T =
        transaction(EasyTransferDatabase.db, statement)

fun <T> inSerializableTransaction(statement: Transaction.() -> T): T =
        transaction(Connection.TRANSACTION_SERIALIZABLE, 1, EasyTransferDatabase.db, statement)