package io.alessio.easytransfer.persistence

import io.alessio.easytransfer.commons.Amount
import io.alessio.easytransfer.commons.EntityNotFoundException
import io.alessio.easytransfer.domain.model.account.Account
import io.alessio.easytransfer.domain.model.account.AccountId
import io.alessio.easytransfer.domain.model.account.AccountRepository
import io.alessio.easytransfer.domain.model.account.AccountStatus
import io.alessio.easytransfer.domain.model.user.UserId
import org.jetbrains.exposed.sql.ResultRow
import org.jetbrains.exposed.sql.Table
import org.jetbrains.exposed.sql.datetime
import org.jetbrains.exposed.sql.insert
import org.jetbrains.exposed.sql.select
import org.jetbrains.exposed.sql.update
import org.joda.time.DateTime
import java.util.*

object AccountTable : Table() {
    val accountId = uuid("account_id").primaryKey()
    val status = varchar("state", 10)
    val currentBalance = decimal("current_balance", 10, 2)
    val createdOn = datetime("created_on")
    val updatedOn = datetime("updated_on")
    val linkedUser = uuid("user_id").nullable()
}

val accountMapper: (ResultRow) -> Account = { row ->
    Account(AccountId(id = row[AccountTable.accountId]),
            currentBalance = Amount.fromBigDecimal(row[AccountTable.currentBalance]),
            state = AccountStatus.valueOf(row[AccountTable.status]),
            linkedUser = if (row[AccountTable.linkedUser] !=null) UserId(row[AccountTable.linkedUser]!!) else null
    ).also {
        it.createdAt = row[AccountTable.createdOn]
        it.updateAt = row[AccountTable.updatedOn]
    }
}

class SqlAccountRepository : AccountRepository {

    override fun getById(accountId: AccountId): Account {
        val account: Account? = inTransaction {
            AccountTable.select { AccountTable.accountId eq accountId.id }
                    .map(accountMapper).singleOrNull()
        }
        return account ?: throw EntityNotFoundException("Cannot find account with id $accountId")
    }

    override fun create(account: Account): Account {
        return inTransaction {
            val now = DateTime.now()
            account.createdAt = now
            account.updateAt = now
            AccountTable.insert {
                it[accountId] = account.id.id
                it[status] = account.state.code
                it[createdOn] = account.createdAt
                it[updatedOn] = account.updateAt
                it[currentBalance] = account.currentBalance.toBigDecimal()
                it[linkedUser] = account.linkedUser()?.id
            }
            account
        }
    }

    override fun update(account: Account) {
        return inTransaction {
            AccountTable.update({ AccountTable.accountId eq account.id.id }) {
                it[status] = account.state.code
                it[updatedOn] = DateTime.now()
                it[currentBalance] = account.currentBalance.toBigDecimal()
                it[linkedUser] = account.linkedUser()?.id
            }
        }
    }

    override fun nextIdentity(): AccountId {
        return AccountId(UUID.randomUUID())
    }

}