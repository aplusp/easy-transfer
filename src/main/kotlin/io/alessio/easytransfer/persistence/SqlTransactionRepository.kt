package io.alessio.easytransfer.persistence

import io.alessio.easytransfer.commons.Amount
import io.alessio.easytransfer.commons.EntityNotFoundException
import io.alessio.easytransfer.domain.model.account.AccountId
import io.alessio.easytransfer.domain.model.transaction.Transaction
import io.alessio.easytransfer.domain.model.transaction.TransactionId
import io.alessio.easytransfer.domain.model.transaction.TransactionLeg
import io.alessio.easytransfer.domain.model.transaction.TransactionLegId
import io.alessio.easytransfer.domain.model.transaction.TransactionRepository
import io.alessio.easytransfer.domain.model.transaction.TransactionState
import org.jetbrains.exposed.dao.UUIDTable
import org.jetbrains.exposed.sql.ResultRow
import org.jetbrains.exposed.sql.datetime
import org.jetbrains.exposed.sql.insert
import org.jetbrains.exposed.sql.select
import org.jetbrains.exposed.sql.update
import org.joda.time.DateTime
import java.util.*

object TransactionTable : UUIDTable() {
    val reference = varchar("reference", 200)
    val state = varchar("state", 200)
    val createdAt = datetime("created_at")
    val updatedAt = datetime("updated_at")
    val completedAt = datetime("completed_at").nullable()
    val failureReason = varchar("failure_reason", 200).nullable()
}

object TransactionLegTable : UUIDTable() {
    val transactionId = uuid("transaction_id").references(TransactionTable.id)
    val amount = decimal("amount", 10, 2)
    val accountId = uuid("account_id")
}


class SqlTransactionRepository : TransactionRepository {

    companion object {
        // Join table
        val TransactionWithLegs = TransactionTable leftJoin TransactionLegTable
    }

    override fun findTransactionsForAccount(accountId: AccountId): Collection<Transaction> {
        return inTransaction {
            TransactionWithLegs
                    .select { TransactionLegTable.accountId eq accountId.id }
                    .toTransactions()
        }
    }

    @Throws(EntityNotFoundException::class)
    override fun getById(transactionId: TransactionId): Transaction {
         val transaction = inTransaction {
            TransactionWithLegs.select { TransactionTable.id eq transactionId.identifier }
                    .toTransactions().singleOrNull()
        }
        return transaction ?: throw EntityNotFoundException("Transaction with id $transactionId not found")
    }

    override fun nextIdentity(): TransactionId {
        return TransactionId(UUID.randomUUID())
    }

    override fun update(transaction: Transaction): Transaction {
        val now = DateTime.now()
        return inTransaction {

            val copy = transaction.copy(updatedAt = now)
            val updatedRows = TransactionTable.update({ TransactionTable.id eq transaction.transactionId.identifier }) {
                it[reference] = copy.reference
                it[failureReason] = copy.failureReason
                it[state] = copy.state.name
                it[updatedAt] = copy.updatedAt
                it[completedAt] = copy.completedAt
            }
            if (updatedRows == 0) {
                throw EntityNotFoundException("Transaction with id ${transaction.transactionId} not found")
            }
            transaction
        }
    }

    override fun create(transaction: Transaction): Transaction {
        val now = DateTime.now()
        return inTransaction {
            val savedTransaction = TransactionTable.insert {
                it[reference] = transaction.reference
                it[state] = transaction.state.name
                it[createdAt] = now
                it[updatedAt] = now
                it[completedAt] = transaction.completedAt
            }[TransactionTable.id].let {
                transaction.copy(
                        transactionId = TransactionId(it.value),
                        createdAt = now,
                        updatedAt = now)
            }
            val savedLegs = savedTransaction.legs.map { leg ->
                val savedLeg = TransactionLegTable.insert {
                    it[amount] = leg.amount.value
                    it[accountId] = leg.accountId.id
                    it[transactionId] = savedTransaction.transactionId.identifier
                }[TransactionLegTable.id].let {
                    leg.copy(legId = TransactionLegId(it.value))
                }
                savedLeg
            }
            savedTransaction.copy(legs = savedLegs)
        }
    }
}

private fun Iterable<ResultRow>.toTransactions(): List<Transaction> {
    return fold(mutableMapOf<TransactionId, Transaction>()) { map, resultRow ->
        val transaction = resultRow.toTransaction()
        val transactionLegId = resultRow.getOrNull(TransactionLegTable.id)
        val transactionLeg = transactionLegId?.let { resultRow.toTransactionLeg() }
        val current = map.getOrDefault(transaction.transactionId, transaction)
        map[transaction.transactionId] = current.copy(legs = current.legs + listOfNotNull(transactionLeg))
        map
    }.values.toList()
}

private fun ResultRow.toTransactionLeg(): TransactionLeg =
        TransactionLeg(legId = TransactionLegId(this[TransactionLegTable.id].value),
                accountId = AccountId(this[TransactionLegTable.accountId]),
                amount = Amount.fromBigDecimal(this[TransactionLegTable.amount]))


private fun ResultRow.toTransaction(): Transaction =
        Transaction(transactionId = TransactionId(this[TransactionTable.id].value),
                reference = this[TransactionTable.reference],
                createdAt = this[TransactionTable.createdAt],
                updatedAt = this[TransactionTable.updatedAt],
                completedAt = this[TransactionTable.completedAt],
                failureReason = this[TransactionTable.failureReason],
                state = TransactionState.valueOf(this[TransactionTable.state]))

