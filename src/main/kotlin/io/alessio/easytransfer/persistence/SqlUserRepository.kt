package io.alessio.easytransfer.persistence

import io.alessio.easytransfer.commons.EntityNotFoundException
import io.alessio.easytransfer.domain.model.account.AccountId
import io.alessio.easytransfer.domain.model.user.User
import io.alessio.easytransfer.domain.model.user.UserId
import io.alessio.easytransfer.domain.model.user.UserRepository
import org.jetbrains.exposed.sql.JoinType
import org.jetbrains.exposed.sql.ResultRow
import org.jetbrains.exposed.sql.Table
import org.jetbrains.exposed.sql.datetime
import org.jetbrains.exposed.sql.insert
import org.jetbrains.exposed.sql.select
import java.util.*

object UserTable : Table() {
    val id = uuid("id").primaryKey()
    val email = varchar("email", 100).uniqueIndex()
    val phoneNumber = varchar("phone_number", 100)
    val firstName = varchar("first_name", 100)
    val lastName = varchar("last_name", 100)
    val createdAt = datetime("created_at")
}

class SqlUserRepository : UserRepository {

    companion object {
        val UserWithAccount = (UserTable.join(AccountTable, JoinType.LEFT,
                additionalConstraint = { UserTable.id eq AccountTable.linkedUser }))
    }

    override fun create(user: User): User {
        return inTransaction {
            UserTable.insert {
                it[id] = user.id.id
                it[email] = user.email
                it[phoneNumber] = user.phoneNumber
                it[createdAt] = user.createdAt
                it[firstName] = user.firstName
                it[lastName] = user.lastName
            }
            user
        }
    }

    override fun getById(userId: UserId): User {
        return ensureExists(inTransaction {
            UserWithAccount.select { UserTable.id eq userId.id }
                    .toUsers().singleOrNull()
        }) { "User with id $userId not found" }
    }

    override fun existsByEmail(email: String): Boolean {
        return inTransaction {
            UserWithAccount.select { UserTable.email eq email }
                    .count() > 0
        }
    }

    override fun nextIdentity(): UserId {
        return UserId(UUID.randomUUID())
    }

    private fun ensureExists(user: User?, messageSupplier: () -> String): User {
        return user ?: throw EntityNotFoundException(messageSupplier())
    }
}

private fun Iterable<ResultRow>.toUsers(): List<User> {
    return fold(mutableMapOf<UserId, User>()) { map, resultRow ->
        val user = resultRow.toUser()
        val account = resultRow.getOrNull(AccountTable.accountId)
        val accountId = account?.let { AccountId(account) }
        val current = map.getOrDefault(user.id, user)
        map[user.id] = current.copy(accounts = current.accounts + listOfNotNull(accountId))
        map
    }.values.toList()
}

private fun ResultRow.toUser(): User {
    return User(
            id = UserId(this[UserTable.id]),
            firstName = this[UserTable.firstName],
            lastName = this[UserTable.lastName],
            phoneNumber = this[UserTable.phoneNumber],
            email = this[UserTable.email],
            accounts = mutableListOf(),
            createdAt = this[UserTable.createdAt]
    )
}
