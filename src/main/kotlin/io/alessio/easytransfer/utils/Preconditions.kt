package io.alessio.easytransfer.utils


object Preconditions {

    /**
     * Throws an [IllegalArgumentException] with message [message] if condition provided by [conditionSupplier]
     * is not satisfied
     */
    fun assert(conditionSupplier: () -> Boolean, message: String) {
        if (!conditionSupplier.invoke()) {
            throw IllegalArgumentException(message)
        }
    }
}