package io.alessio.easytransfer.commons

import spock.lang.Specification

class AmountSpec extends Specification {

    def "two sum are added as expected"() {

        expect:
        use(AmountKt) {
            result == a + b
        }

        where:
        a                 | b                     | result
        Amount.of(10, 20) | Amount.of(1, 23)      | Amount.of(11, 43)
        Amount.of(10, 20) | Amount.of(1, 2333)    | Amount.of(11, 43)
        Amount.of(10, 20) | Amount.of(1, 2333000) | Amount.of(11, 43)
        Amount.of(0, 20)  | Amount.of(1, 236)     | Amount.of(1, 44)
    }

    def "string parsed to amount"() {
        when:
        def amount = Amount.fromString(stringValue)

        then:
        amount == result

        where:
        stringValue | result
        "123.678"   | Amount.of(123, 68)
        "123"       | Amount.of(123, 0)
        "0.78"      | Amount.of(0, 78)
        "08.780"    | Amount.of(8, 78)
    }

    def "illegal argument exception when unparseable amount"() {
        when:
        Amount.fromString(stringValue)

        then:
        thrown(expectedException)

        where:
        stringValue | expectedException
        "123.678."  | IllegalArgumentException
        ""          | IllegalArgumentException
    }

    def "amount sign manipulation - positive"() {
        when:
        def positive = value.positiveValue

        then:
        positive == expected

        where:
        value             | expected
        Amount.of(-23, 0) | Amount.of(23, 0)
        Amount.of(23, 0)  | Amount.of(23, 0)
    }

}
