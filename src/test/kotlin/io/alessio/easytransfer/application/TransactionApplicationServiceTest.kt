package io.alessio.easytransfer.application

import io.alessio.easytransfer.commons.Amount
import io.alessio.easytransfer.domain.model.account.AccountRepository
import io.alessio.easytransfer.domain.model.transaction.DepositCommand
import io.alessio.easytransfer.domain.model.transaction.TransactionFailures
import io.alessio.easytransfer.domain.model.transaction.TransactionRepository
import io.alessio.easytransfer.domain.model.transaction.TransactionState
import io.alessio.easytransfer.domain.model.transaction.TransferCommand
import io.alessio.easytransfer.mothers.AccountMother
import io.alessio.easytransfer.persistence.BaseSqlTest
import io.alessio.easytransfer.persistence.SqlAccountRepository
import io.alessio.easytransfer.persistence.SqlTransactionRepository
import org.junit.Assert.assertEquals
import org.junit.Assert.assertNotNull
import org.junit.Before
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertThrows

class TransactionApplicationServiceTest : BaseSqlTest() {

    private lateinit var transactionApplicationService: TransactionApplicationService
    private lateinit var accountRepository: AccountRepository
    private lateinit var transactionRepository: TransactionRepository

    @BeforeEach
    fun setUp() {
        accountRepository = SqlAccountRepository()
        transactionRepository = SqlTransactionRepository()
        transactionApplicationService = TransactionApplicationService(transactionRepository, accountRepository)
    }

    @Test
    fun executeATransferBetweenTwoAccounts() {

        val sourceAccountCreated = accountRepository.create(AccountMother.activeAccountWithBalance(Amount.of(1000)))
        val targetAccountCreated = accountRepository.create(AccountMother.activeAccountWithBalance(Amount.of(20)))

        val transaction = transactionApplicationService.transfer(TransferCommand(sourceAccountCreated.id,
                targetAccountCreated.id,
                Amount.of(20),
                "reference"))

        assertEquals(TransactionState.SUCCESS, transaction.state)
        assertNotNull(transaction.completedAt)

        val loadedSourceAccount = accountRepository.getById(sourceAccountCreated.id)
        assertEquals(Amount.of(980), loadedSourceAccount.currentBalance)
        val loadedTargetAccount = accountRepository.getById(targetAccountCreated.id)
        assertEquals(Amount.of(40), loadedTargetAccount.currentBalance)

        val loadedTransaction = transactionRepository.getById(transaction.transactionId)
        assertNotNull(loadedTransaction)
    }

    @Test
    fun sendMoneySourceAccountWithoutEnoughFunds() {
        val reference = "reference"
        val sourceAccountCreated = accountRepository.create(AccountMother.activeAccountWithBalance(Amount.of(1000)))
        val targetAccountCreated = accountRepository.create(AccountMother.activeAccountWithBalance(Amount.of(20)))

        val transaction = transactionApplicationService.transfer(TransferCommand(sourceAccountCreated.id,
                targetAccountCreated.id,
                Amount.of(1200),
                reference))

        val loadedTransaction = transactionRepository.getById(transaction.transactionId)
        assertEquals(TransactionState.DECLINED, loadedTransaction.state)
        assertEquals(reference, loadedTransaction.reference)
    }

    @Test
    fun noTransferBetweenSameAccount() {
        val reference = "reference"
        val sourceAccountCreated = accountRepository.create(AccountMother.activeAccountWithBalance(Amount.of(1000)))

        Assertions.assertThrows(IllegalArgumentException::class.java) {
            transactionApplicationService.transfer(TransferCommand(sourceAccountCreated.id,
                    sourceAccountCreated.id,
                    Amount.of(1200),
                    reference))
        }
    }

    @Test
    fun noNegativeAmountTransfers() {
        val reference = "reference"
        val sourceAccountCreated = accountRepository.create(AccountMother.activeAccountWithBalance(Amount.of(1000)))
        val targetAccountCreated = accountRepository.create(AccountMother.activeAccountWithBalance(Amount.of(20)))
        Assertions.assertThrows(IllegalArgumentException::class.java) {
            transactionApplicationService.transfer(TransferCommand(sourceAccountCreated.id,
                    targetAccountCreated.id,
                    Amount.of(-1200),
                    reference))
        }
    }

    @Test
    fun aTransferToOrFromInactiveAccountFails() {

        val sourceAccountCreated = accountRepository.create(AccountMother.inactiveAccount(Amount.of(1000)))
        val targetAccountCreated = accountRepository.create(AccountMother.activeAccountWithBalance(Amount.of(20)))

        val transaction = transactionApplicationService.transfer(TransferCommand(sourceAccountCreated.id,
                targetAccountCreated.id,
                Amount.of(1200),
                "reference"))

        val loadedTransaction = transactionRepository.getById(transaction.transactionId)
        assertEquals(TransactionState.FAILED, loadedTransaction.state)
        assertEquals(TransactionFailures.INACTIVE_ACCOUNT.name, loadedTransaction.failureReason)
    }

    @Test
    fun depositFundsToAccount() {
        val account = accountRepository.create(AccountMother.activeAccountWithBalance(Amount.of(1000)))
        val transaction = transactionApplicationService.deposit(DepositCommand(account.id, Amount.of(10), "a reference"))

        val loadedTransaction = transactionRepository.getById(transaction.transactionId)
        assertEquals(TransactionState.SUCCESS, loadedTransaction.state)
        assertEquals(1, transaction.legs.size)
        assertEquals(Amount.of(10), transaction.legs.first().amount)
        assertEquals(account.id, transaction.legs.first().accountId)

        val loadedAccount = accountRepository.getById(account.id)
        assertEquals(Amount.of(1010), loadedAccount.currentBalance)


    }
}