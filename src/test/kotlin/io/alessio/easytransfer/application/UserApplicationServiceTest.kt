package io.alessio.easytransfer.application

import io.alessio.easytransfer.commons.Amount
import io.alessio.easytransfer.domain.model.account.AccountStatus
import io.alessio.easytransfer.domain.model.user.CreateUserCommand
import io.alessio.easytransfer.persistence.BaseSqlTest
import io.alessio.easytransfer.persistence.SqlAccountRepository
import io.alessio.easytransfer.persistence.SqlUserRepository
import org.junit.Assert
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test

class UserApplicationServiceTest : BaseSqlTest() {

    lateinit var userApplicationService: UserApplicationService
    lateinit var accountApplicationService: AccountApplicationService

    @BeforeEach
    fun before() {
        accountApplicationService = AccountApplicationService(SqlAccountRepository())
        userApplicationService = UserApplicationService(SqlUserRepository(), accountApplicationService)
    }

    @Test
    fun whenUserIsCreatedTheRelativeAccountIsCreated() {
        val createUserCommand = CreateUserCommand("email", "Jhon", "Doe", "12345")
        val user = userApplicationService.createUser(createUserCommand)

        val account = accountApplicationService.getAccount(user.accounts.first())
        Assert.assertEquals(Amount.ZERO, account.currentBalance)
        Assert.assertEquals(AccountStatus.ACTIVE, account.state)
    }
}