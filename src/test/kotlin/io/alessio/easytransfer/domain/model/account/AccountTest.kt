package io.alessio.easytransfer.domain.model.account

import io.alessio.easytransfer.commons.Amount
import io.alessio.easytransfer.mothers.AccountMother
import org.junit.Assert.assertEquals
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test

class AccountTest {

    @Test
    fun whenWhitdrawingBalanceIsUpdated() {
        val account = AccountMother.activeAccountWithBalance(Amount.of(1200))
        account.whitdraw(Amount.of(150))
        assertEquals(Amount.of(1050), account.currentBalance)
    }

    @Test
    fun whenWhitdrawingMoreThanTotalBalanceThrows() {
        val account = AccountMother.activeAccountWithBalance(Amount.of(1200))
        Assertions.assertThrows(NotEnoughFundsException::class.java) {
            account.whitdraw(Amount.of(1200, 1))
        }
    }

    @Test
    fun aNonActiveAccountCannotReceiveMoney() {
        val inactiveAccount = AccountMother.inactiveAccount()
        Assertions.assertThrows(InactiveAccountException::class.java) {
            inactiveAccount.deposit(Amount.of(10))
        }
    }
}