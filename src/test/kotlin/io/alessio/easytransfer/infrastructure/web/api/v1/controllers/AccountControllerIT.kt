package io.alessio.easytransfer.infrastructure.web.api.v1.controllers

import org.junit.Assert.assertEquals
import org.junit.jupiter.api.Test
import java.util.*

class AccountControllerIT : BaseHttpTest() {

    @Test
    fun getNotExistingAccount() {
        val response = httpTest.get<UserDto>("/user/${UUID.randomUUID()}")
        assertEquals(404, response.status)
    }

    @Test
    fun getExistingAccount() {
        val user = httpTest.registerRandomUser()
        val response = httpTest.get<UserDto>("/user/${user.userId}")
        assertEquals(200, response.status)
        assertEquals(user.userId, response.body.userId)
        assertEquals(user.emailAddress, response.body.emailAddress)
        assertEquals(user.firstName, response.body.firstName)
        assertEquals(user.lastName, response.body.lastName)
        assertEquals(user.phoneNumber, response.body.phoneNumber)
    }
}