package io.alessio.easytransfer.infrastructure.web.api.v1.controllers

import io.alessio.easytransfer.infrastructure.AppConfig
import io.alessio.easytransfer.infrastructure.DeafultDependencyManager
import io.alessio.easytransfer.utils.HttpTestUtil
import io.alessio.easytransfer.utils.SocketUtils
import io.javalin.Javalin
import org.junit.ClassRule
import org.junit.jupiter.api.AfterAll
import org.junit.jupiter.api.BeforeAll
import org.junit.jupiter.api.TestInstance

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
abstract class BaseHttpTest {
    lateinit var app: Javalin
    lateinit var httpTest: HttpTestUtil
    val dependencyManager = DeafultDependencyManager()

    @ClassRule
    @BeforeAll
    fun beforeAll() {
        app = AppConfig().setup(dependencyManager)
        app.start(SocketUtils.findAvailableTcpPort())
        httpTest = HttpTestUtil(app.port(), dependencyManager.objectMapper)
    }

    @AfterAll
    fun start() {
        app.stop()
    }
}