package io.alessio.easytransfer.infrastructure.web.api.v1.controllers

import io.alessio.easytransfer.domain.model.transaction.TransactionState
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Assertions.assertFalse
import org.junit.jupiter.api.Assertions.assertNotNull
import org.junit.jupiter.api.Assertions.assertTrue
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import java.math.BigDecimal
import java.util.*

class TransactionControllerIT : BaseHttpTest() {

    lateinit var registeredUser: UserDto
    lateinit var userDto: UserDto

    @BeforeEach
    fun beforeEach() {
        userDto = httpTest.registerRandomUser()

        httpTest.post<Int>("/pay", DepositCommandDto(BigDecimal.valueOf(1000),
                userDto.accounts.first().accountId, "deposit"))
    }

    @Test
    fun depositToAnExistingAccount() {
        val response = httpTest.post<TransactionResponseDto>("/pay",
                DepositCommandDto(BigDecimal.valueOf(1000), userDto.accounts.first().accountId, "deposit"))

        assertEquals(200, response.status)
    }

    @Test
    fun depositToNotExistingAccount() {
        val response = httpTest.post<TransactionResponseDto>("/pay",
                DepositCommandDto(BigDecimal.valueOf(1000), UUID.randomUUID().toString(), "deposit"))

        assertEquals(409, response.status)
        assertFalse(response.body.success)
        assertEquals("UNKNOWN_ACCOUNT", response.body.failureReason)
    }

    @Test
    fun transferSuccessfull() {
        val user2 = httpTest.registerRandomUser()
        val transactionResponse = httpTest.post<TransactionResponseDto>("/transfer",
                TransferCommandDto(BigDecimal.valueOf(500),
                        userDto.accounts.first().accountId, user2.accounts.first().accountId, "transfer"))

        assertEquals(200, transactionResponse.status)
        assertTrue(transactionResponse.body.success)
        assertEquals(TransactionState.SUCCESS, transactionResponse.body.detail.state)
        assertNotNull(transactionResponse.body.detail.transactionId)
        assertNotNull(transactionResponse.body.detail.completedAt)
    }

    @Test
    fun transferNotEnoughFundSuccessfull() {
        val user2 = httpTest.registerRandomUser()
        val transactionResponse = httpTest.post<TransactionResponseDto>("/transfer",
                TransferCommandDto(BigDecimal.valueOf(500000),
                        userDto.accounts.first().accountId, user2.accounts.first().accountId, "transfer"))

        assertEquals(409, transactionResponse.status)
        assertFalse(transactionResponse.body.success)
        assertEquals("NOT_ENOUGH_FUNDS", transactionResponse.body.failureReason)
    }

    @Test
    fun deposit() {
    }
}