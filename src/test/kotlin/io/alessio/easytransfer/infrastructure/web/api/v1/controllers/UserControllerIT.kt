package io.alessio.easytransfer.infrastructure.web.api.v1.controllers

import io.alessio.easytransfer.domain.model.user.CreateUserCommand
import org.junit.Assert.assertEquals
import org.junit.Assert.assertNotNull
import org.junit.jupiter.api.Test

class UserControllerIT: BaseHttpTest() {

    @Test
    fun createNewUser() {
        val email = "jhon2@email.it"
        val phoneNumber = "7490875673"
        val firstName = "Jhon"
        val lastName = "Doe"
        val response = httpTest.post<UserDto>("/user", CreateUserCommand(email, firstName, lastName, phoneNumber))
        assertEquals(201, response.status)
        assertEquals(1, response.body.accounts.size)
        assertNotNull(response.body.userId)
        assertEquals(email, response.body.emailAddress)
        assertEquals(firstName, response.body.firstName)
        assertEquals(lastName, response.body.lastName)
        assertEquals(email, response.body.emailAddress)
    }

    @Test
    fun tryingToDuplicateEmailUserThenBadRequest() {
        val email = "jhon2@email.it"
        val phoneNumber = "7490875673"
        val firstName = "Jhon"
        val lastName = "Doe"
        httpTest.post<UserDto>("/user", CreateUserCommand(email, firstName, lastName, phoneNumber))
        val response = httpTest.post<UserDto>("/user", CreateUserCommand(email, firstName, lastName, phoneNumber))
        assertEquals(400, response.status)
    }

    @Test
    fun invalidUserEmailThenBadRequest() {
        val email = "jhon2"
        val phoneNumber = "7490875673"
        val firstName = "Jhon"
        val lastName = "Doe"
        val response = httpTest.post<UserDto>("/user", CreateUserCommand(email, firstName, lastName, phoneNumber))
        assertEquals(400, response.status)
    }

    @Test
    fun missingMandatoryField() {
        val email = ""
        val phoneNumber = "7490875673"
        val firstName = ""
        val lastName = "Doe"
        val response = httpTest.post<UserDto>( "/user", CreateUserCommand(email, firstName, lastName, phoneNumber))
        assertEquals(400, response.status)
    }
}