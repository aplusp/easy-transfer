package io.alessio.easytransfer.mothers

import io.alessio.easytransfer.commons.Amount
import io.alessio.easytransfer.domain.model.account.Account
import io.alessio.easytransfer.domain.model.account.AccountId
import io.alessio.easytransfer.domain.model.account.AccountStatus
import io.alessio.easytransfer.domain.model.user.UserId
import java.util.*

class AccountMother private constructor() {
    companion object {
        fun activeAccount(): Account = activeAccountWithBalance(Amount.of(2000))
        fun activeAccountWithUser(userId: UserId): Account = activeAccountWithBalanceAndUserUser(userId, Amount.of(2000))

        fun activeAccountWithBalance(balance: Amount): Account {
            return baseAccount().also {
                it.state = AccountStatus.ACTIVE
                it.currentBalance = balance
            }
        }

        fun activeAccountWithBalanceAndUserUser(userId: UserId, balance: Amount): Account {
            return baseAccount(userId = userId).also {
                it.state = AccountStatus.ACTIVE
                it.currentBalance = balance
            }
        }

        fun inactiveAccount(balance: Amount = Amount.ZERO): Account {
            return baseAccount().also {
                it.state = AccountStatus.SUSPENDED
                it.currentBalance = balance
            }
        }

        private fun baseAccount(uuid: UUID = UUID.randomUUID(), userId: UserId = UserId(UUID.randomUUID())): Account {
            return Account(
                    id = AccountId(uuid),
                    currentBalance = Amount.of(0),
                    linkedUser = userId
            )
        }
    }
}