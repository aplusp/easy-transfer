package io.alessio.easytransfer.persistence

import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.BeforeEach

abstract class BaseSqlTest {

    lateinit var databaseFactory: DefaultDatabaseFactory

    @BeforeEach
    fun setup() {
        databaseFactory = DefaultDatabaseFactory("jdbc:h2:mem:easytransfersTest;DB_CLOSE_DELAY=-1")
        EasyTransferDatabase.init(databaseFactory)
        databaseFactory.initializeSchemas()
    }

    @AfterEach
    fun after() {
        inTransaction {
            exec("drop all objects")
        }
    }
}