package io.alessio.easytransfer.persistence

import io.alessio.easytransfer.commons.Amount
import io.alessio.easytransfer.commons.EntityNotFoundException
import io.alessio.easytransfer.domain.model.account.Account
import io.alessio.easytransfer.domain.model.account.AccountId
import io.alessio.easytransfer.domain.model.account.AccountRepository
import io.alessio.easytransfer.domain.model.user.UserId
import org.junit.Assert.assertEquals
import org.junit.Assert.assertNotNull
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test
import java.util.*

class SqlAccountRepositoryTest : BaseSqlTest() {
    private val repository: AccountRepository = SqlAccountRepository()

    @Test
    fun accountIsCreatedCorrectly() {
        val account = createAccount()
        assertNotNull(account.id)
        assertNotNull(account.createdAt)
        assertNotNull(account.updateAt)
    }

    @Test
    fun accountIsLoadedCorrectly() {
        val account = createAccount()

        val loadedAccount = repository.getById(account.id)
        assertNotNull(loadedAccount.state)
        assertNotNull(loadedAccount.createdAt)
        assertNotNull(loadedAccount.updateAt)
        assertNotNull(loadedAccount.currentBalance)
    }


    @Test
    fun createAccountAndLinkUser() {

        val account = createAccount()
        val userId = UserId(UUID.randomUUID())
        account.linkUser(userId)
        repository.update(account)

        val loadedAccount = repository.getById(account.id)
        assertNotNull(loadedAccount.linkedUser())
        assertEquals(userId, loadedAccount.linkedUser())
    }


    private fun createAccount(): Account {
        val newAccount = Account(id = AccountId(UUID.randomUUID()), currentBalance = Amount.of(0))
        return repository.create(newAccount)
    }


    @Test
    fun whenAccountNotFoundThrows() {
       Assertions.assertThrows(EntityNotFoundException::class.java){
           repository.getById(AccountId(UUID.randomUUID()))
       }
    }

}