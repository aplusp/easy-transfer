package io.alessio.easytransfer.persistence

import io.alessio.easytransfer.commons.Amount
import io.alessio.easytransfer.domain.model.account.AccountId
import io.alessio.easytransfer.domain.model.transaction.Transaction
import io.alessio.easytransfer.domain.model.transaction.TransactionRepository
import io.alessio.easytransfer.domain.model.transaction.TransferCommand
import org.junit.Assert.assertEquals
import org.junit.Assert.assertNotNull
import org.junit.jupiter.api.Test
import java.util.*

class SqlTransactionRepositoryTest: BaseSqlTest() {

    private val repository: TransactionRepository = SqlTransactionRepository()


    @Test
    fun saveTransaction() {
        val savedTransaction = createTransaction()

        assertNotNull(savedTransaction.transactionId)
        assertNotNull(savedTransaction.createdAt)
        assertNotNull(savedTransaction.updatedAt)
        assertEquals(2, savedTransaction.legs.size)
        savedTransaction.legs.forEach {
            assertNotNull("Transaction leg id should not be null", it.legId)
        }

        val loadedTransaction = repository.getById(savedTransaction.transactionId)
        assertNotNull(loadedTransaction)
        assertEquals(2, loadedTransaction.legs.size)
        loadedTransaction.legs.forEach {
            assertNotNull("Transaction leg id should not be null", it.legId)
            assertNotNull(it.accountId)
            assertNotNull(it.amount)
        }
    }

    @Test
    fun loadTransactionByAccount() {
        val fromAccountId = AccountId(UUID.randomUUID())
        val toAccountId = AccountId(UUID.randomUUID())
        createTransaction(from = fromAccountId, to = toAccountId)

        val loadedTransactions = repository.findTransactionsForAccount(fromAccountId)

        assertEquals(1, loadedTransactions.size)
        loadedTransactions.iterator().next()
    }

    fun createTransaction(from: AccountId = AccountId(UUID.randomUUID()), to: AccountId = AccountId(UUID.randomUUID())): Transaction {
        val transaction = Transaction.buildForTransfer(repository.nextIdentity(), TransferCommand(from, to, Amount.of(10), "test"))
        return repository.create(transaction)
    }
}