package io.alessio.easytransfer.persistence

import io.alessio.easytransfer.commons.EntityNotFoundException
import io.alessio.easytransfer.domain.model.account.AccountId
import io.alessio.easytransfer.domain.model.account.AccountRepository
import io.alessio.easytransfer.domain.model.user.User
import io.alessio.easytransfer.domain.model.user.UserRepository
import io.alessio.easytransfer.mothers.AccountMother
import org.junit.Assert
import org.junit.jupiter.api.Test
import java.sql.SQLException
import java.util.*

class SqlUserRepositoryTest : BaseSqlTest() {

    private val userRepository: UserRepository = SqlUserRepository()
    private val accountRepository: AccountRepository = SqlAccountRepository()

    @Test
    fun createUser() {
        val user = User(userRepository.nextIdentity(), "email", "name", "surname", "9090909", mutableListOf())
        val create = userRepository.create(user)
        Assert.assertNotNull(create.createdAt)
    }

    @Test
    fun getUserLinkedToAccountAccountId() {
        val email = "email@email.it"
        val user = userRepository.create(User(userRepository.nextIdentity(),
                email,
                "name",
                "surname",
                "9090909"))
        val createdAccount = accountRepository.create(AccountMother.activeAccountWithUser(user.id))
        val loadedUser = userRepository.getById(user.id)
        Assert.assertEquals(1, loadedUser.accounts.size)
        Assert.assertEquals(createdAccount.id, loadedUser.accounts.first())
    }

    @Test
    fun createUserWithAccountIdNotExistingShouldThrow() {
        val email = "email@email.it"
        val identity = userRepository.nextIdentity()
        val user = User(identity,
                email,
                "name",
                "surname",
                "9090909",
                mutableListOf(AccountId(UUID.randomUUID())))

        try {
            // Here should throw a constraint violation exception
            userRepository.create(user)
        } catch (ex: Exception) {
            Assert.assertTrue(ex is SQLException)

        }

        try {
            // It should have rolled back thus account should not be found
            userRepository.getById(identity)
        } catch (ex: Exception) {
            Assert.assertTrue(ex is EntityNotFoundException)
        }
    }

}