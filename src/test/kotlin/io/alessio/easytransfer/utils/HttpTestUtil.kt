package io.alessio.easytransfer.utils


import com.fasterxml.jackson.databind.ObjectMapper
import io.alessio.easytransfer.domain.model.user.CreateUserCommand
import io.alessio.easytransfer.infrastructure.web.api.v1.controllers.UserDto
import io.javalin.core.util.Header
import kong.unirest.JacksonObjectMapper
import kong.unirest.Unirest
import java.net.InetAddress
import java.util.*
import javax.net.ServerSocketFactory


class HttpTestUtil(port: Int, objectMapper: ObjectMapper) {
    private val json = "application/json"
    val headers = mutableMapOf(Header.ACCEPT to json, Header.CONTENT_TYPE to json)

    init {
        Unirest.config().objectMapper = JacksonObjectMapper(objectMapper)
    }

    @JvmField
    val origin: String = "http://localhost:$port"

    inline fun <reified T> post(path: String) =
            Unirest.post(origin + path).headers(headers).asObject(T::class.java)

    inline fun <reified T> post(path: String, body: Any) =
            Unirest.post(origin + path).headers(headers).body(body).asObject(T::class.java)

    inline fun <reified T> get(path: String, params: Map<String, Any>? = null) =
            Unirest.get(origin + path).headers(headers).queryString(params).asObject(T::class.java)


    fun registerUser(email: String, phoneNumber: String, firstName: String, lastName: String): UserDto {
        val response = post<UserDto>("/user", CreateUserCommand(email, firstName, lastName, phoneNumber))
        return response.body
    }

    fun registerRandomUser(): UserDto {
        val response = post<UserDto>("/user",
                CreateUserCommand("${UUID.randomUUID().toString().substring(1, 10)}@email.it",
                        "name",
                        "lastname",
                        "4567890242"))
        return response.body
    }

}


/**
 * Courtesy
 */
object SocketUtils {
    const val PORT_RANGE_MIN = 1024
    const val PORT_RANGE_MAX = 65535
    private val random = Random(System.currentTimeMillis())

    @JvmOverloads
    fun findAvailableTcpPort(minPort: Int = PORT_RANGE_MIN, maxPort: Int = PORT_RANGE_MAX): Int {
        return findAvailablePort(minPort, maxPort)
    }

    private fun isPortAvailable(port: Int): Boolean {
        return try {
            val serverSocket = ServerSocketFactory.getDefault().createServerSocket(
                    port, 1, InetAddress.getByName("localhost"))
            serverSocket.close()
            true
        } catch (ex: Exception) {
            false
        }
    }


    private fun findRandomPort(minPort: Int, maxPort: Int): Int {
        val portRange = maxPort - minPort
        return minPort + random.nextInt(portRange + 1)
    }

    fun findAvailablePort(minPort: Int, maxPort: Int): Int {
        val portRange = maxPort - minPort
        var candidatePort: Int
        var searchCounter = 0
        do {
            check(searchCounter <= portRange)
            candidatePort = findRandomPort(minPort, maxPort)
            searchCounter++
        } while (!isPortAvailable(candidatePort))
        return candidatePort
    }
}